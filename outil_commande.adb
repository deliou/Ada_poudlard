with ada.text_io, ada.integer_text_io, ada.Integer_Text_IO, Ada.Numerics.Elementary_Functions;
use ada.text_io, ada.integer_text_io, ada.Integer_Text_IO;

PACKAGE BODY Outil_Commande IS

   PROCEDURE Affiche_Article(Article: IN T_Article) IS

   BEGIN
      FOR Article IN T_Article LOOP
         Put(T_Article'Image(Article));
         Put(",");
         END LOOP;
   END Affiche_Article;

     PROCEDURE Saisie_text (mot : OUT T_mot) IS
   BEGIN
      LOOP
         BEGIN
            Get(mot);
            Skip_Line;
            EXIT;
         EXCEPTION
            WHEN Data_Error =>
               Skip_Line;
               Put_Line(" erreur de saisie, veuillez recommencer ");
            WHEN Constraint_Error =>
               Skip_Line;
               Put_Line(" mauvaise valeur, recommencez ");
         END;
      END LOOP;
   END Saisie_Text;

   PROCEDURE Affiche_Text(mot :IN T_Mot) IS
      K:Integer;
      s:string(1..30);
   BEGIN
      loop
   BEGIN
      Put_Line("saisie le mot");
     get_line(s,k);
       EXIT;
         EXCEPTION
            WHEN Data_Error =>
               Skip_Line;
               Put_Line(" erreur de saisie, veuillez recommencer ");
            WHEN Constraint_Error =>
               Skip_Line;
               Put_Line(" mauvaise valeur, recommencez ");
   END;
   end loop;
END Affiche_Text;

      procedure affiche_nombre (n : in integer) is
	base : integer;
Begin

	if n< 0 then
		base := integer (Ada.Numerics.Elementary_Functions.Log(float(-n) +1.0, 10.0));--ici j'ai fait comme toi, mais je pense qu'il faut enlever le float parce qu'on travaille qu'avc les entiers--
		base := base +1;
	else
		base := integer (Ada.Numerics.Elementary_Functions.Log(float(n) +1.0, 10.0));
	end if;

	put (n, base);
end affiche_nombre;
   end outil_commande;
