with ada.text_io, ada.integer_text_io, ada.Integer_Text_IO, Ada.Numerics.Elementary_Functions;
use ada.text_io, ada.integer_text_io, ada.Integer_Text_IO;

PACKAGE outil_commande IS

   TYPE T_Article IS (Baguette_Magique,Dragibus,Cape_Invisibilite,Plumes_Papote,Balais,Robe_Sorcier);

   subtype T_mot is string(1..30);


   PROCEDURE Affiche_Article(Article: IN T_Article);
   PROCEDURE Saisie_Text(mot: OUT T_mot);
   PROCEDURE Affiche_Text(Mot: IN T_Mot);
   Procedure affiche_nombre(n: IN integer);



end outil_commande;