WITH Gestion_Livreur, Outil_Commande;
Use gestion_livreur, outil_commande;

Package gestion_commande is

   TYPE T_Statut_Com IS (Attente, Preparation, Attente_Reglement, Reglee, Annulee);

   TYPE T_Tab_DetailCom IS ARRAY (T_Article )OF Integer; --quantite de chaque produit


Type T_Cell_Com;
Type T_Pteur_Com is access T_Cell_Com;
Type T_Cell_Com is record
	Nom_C : T_mot;
	Id : integer;
	Secteur : T_Secteur;
	Statut : T_statut_com;
	Detail_Com : T_Tab_DetailCom;
	Facture : integer;
	Nom_Livreur : T_mot;
	suiv : T_Pteur_Com;
END RECORD;

Type T_File_com is record
	Tete, Fin : T_Pteur_Com;
END RECORD;

Les_Prix : CONSTANT T_Tab_DetailCom := (10, 25, 75, 10, 58, 67);

TYPE T_TabCom IS ARRAY (T_Secteur, T_Statut_Com) OF T_Pteur_Com;

Procedure Creation_Commande ( file_non_payee : IN OUT T_file_com; Id_Com : IN OUT integer);


End gestion_commande;
