WITH Ada.Text_IO, Ada.Integer_Text_IO, Gestion_Livreur, Outil_Commande;
Use ada.Text_IO, ada.Integer_Text_IO, gestion_livreur, outil_commande;

Package body gestion_commande is

Procedure Creation_Commande ( file_non_payee : IN OUT T_file_com; Id_Com : IN OUT integer) is
com : T_Pteur_Com;
k, choix_com, Id_Com, facture_tot : integer;
total : integer := 0;
detail : T_Tab_DetailCom;

Begin

Put ("Veuillez entrer le nom du client : ");
Get_line (com.Nom_C, k);

Put ("Voici les secteurs disponibles : "); new_line;
      FOR I IN NE .. SO LOOP
         Put(T_Secteur'Pos(I) +1);
         Put (" : ");
         Put(T_Secteur'Image(I));
         New_Line;
      END LOOP;

      LOOP
         Put ("Veuillez saisir le chiffre du secteur de votre commande : ");
         Get (choix_com);
         EXIT WHEN choix_com IN 1..6;
      END LOOP;
      com.secteur := T_Secteur'Val(choix_com -1);


      Put ("Veuillez entrer la composition de la commande : ");
      New_Line;
      FOR I IN T'RANGE LOOP
         Put(T_article'Image(I));
         Put("( ");
         Put(Les_Prix(I), 2);
         Put(" euros");
         Put(" ) ");
         Put(" => ");
         Get (T(I));
      END LOOP;
      Com.Detail_Com := detail;

      Id_Com := Id_Com + 1;
      Com.Id := Id_Com;
      Put ("L�identifiant de la commande est : ");
      Put (Com.Id, 2);
      New_Line;
FOR I IN T'RANGE LOOP
         Total := Total + Les_Prix(I) * T(I);
      END LOOP;
      Com.Facture := Total;
      Put ("Le prix total de la commande sans les charges est : ");
      Put(Com.Facture, 2);
      Put (" Euros");
      New_Line;
      Put ("Le montant de votre facture s'eleve donc a : ");
      IF ((Total * 6)/100) <= 20 THEN
         Facturetot := Total + ((Total * 6)/100);
      ELSE Facturetot := Total + 20;
      END IF;
      Put(Facturetot);
      Put(" Euros");New_Line;
      Com.Facture := Facturetot;
      New_Line;

End Creation_Commande;
-- Je n�ai pas d�fini quel livreur prend la commande !

PROCEDURE Affiche_Commande (
         F_attente,
         F_preparation,
         F_attenteR, F_reglee, F_annulee : IN     T_File) IS
      Choix_F : Integer;
      Com       : T_Commande;
      detail : T_Tab_DetailCom;
      Pt        : T_PteurCom;

   BEGIN

      Put ("Quel est le statut de la commande que vous souhaitez visualiser ?");
      New_Line;
      FOR I IN Attente .. Annulee LOOP
         Put(Statut'Pos(I) +1);
         Put (" : ");
         Put(Statut'Image(I));
         New_Line;
      END LOOP;
      New_Line;
      LOOP
         Put ("Veuillez saisir le chiffre correspondant : ");
         Get (Choix_F);
         New_Line;
         EXIT WHEN Choix_F IN 1..5;
      END LOOP;

      IF Choix_F = 1 THEN
         Pt := F_attente.Tete;
      ELSIF Choix_F =2 THEN
         Pt :=F_preparation.Tete;
      ELSIF Choix_F =3 THEN
         Pt :=F_attenteR.Tete;
 ELSIF Choix_F = 4 THEN
         Pt :=F_reglee.Tete;
 ELSIF Choix_F = 5 THEN
         Pt :=F_annulee.Tete;
      END IF;

      IF Pt /= NULL THEN
         WHILE Pt /= NULL LOOP
            Put ("Nom du client : ");
            Put (Pt.Val.Nom_C);
            New_Line;
            Put ("Secteur : ");
            Put(T_Secteur'Image(Pt.Val.Secteur));
            New_Line;
            Put ("Montant total de la commande : ");
            Put(Pt.Val.Facture, 2);
            New_Line;
            Put ("Identifiant de la commande : ");
            Put (Pt.Val.Id, 2);
            New_Line;
            IF Pt.Val.Nom_Livreur(1) /=' ' THEN
               Put ("Nom du livreur : ");
               Put(Pt.Val.Nom_Livreur);
               New_Line;
            END IF;
            Put ("Detail de la commande : ");
            New_Line;
            FOR I IN T'RANGE LOOP
               Put(T_Article'Image(I));
               Put(" ");
               Put(Pt.Val.Detail(I), 2);
               New_Line;
            END LOOP;
            Pt := Pt.Suiv;
            New_Line;
         END LOOP;
      ELSE
         IF Pt = NULL THEN
            Put ("Liste vide.");
         END IF;
      END IF;
   END Affiche_Commande;

End gestion_commande;
