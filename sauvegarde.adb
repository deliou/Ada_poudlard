with outil_commande, ada.text_io;
use outil_commande, ada.text_io;


package body sauvegarde is

	function init_tab_nombre_liv (tab : in T_tab_liv) return T_tab_nombre_liv is
		t : T_tab_nombre_liv;
	begin
		for sec in T_secteur'range loop
			for stat in T_statut_liv'range loop
				t (sec, stat) := nb_liv_liste (tab (sec, stat));
			end loop;
		end loop;
		return t;
	end init_tab_nombre_liv;

	procedure aff_tab_nombre_liv (t : T_tab_nombre_liv) is
		
	begin
		for sec in T_secteur'range loop
			for stat in T_statut_liv'range loop
				affiche_nombre (t(sec, stat));
			end loop;
		end loop;
	end aff_tab_nombre_liv;
	
	procedure sauv_tab_nombre_liv (t : in T_tab_nombre_liv) is
		F : fich_tab_nombre_liv.File_type;
		nom_fich : constant string := "sauv_nombre_liv.sav";
	begin
		begin
			open(F, out_file, nom_fich);
			Exception
			When others => Create(F, name=>nom_fich);
		end;
	write(F, t);
	close(F);
	end sauv_tab_nombre_liv;
	
	function charge_tab_nombre_liv return T_tab_nombre_liv is
		t : T_tab_nombre_liv;
		F : fich_tab_nombre_liv.File_type;
		nom_fich : constant string := "sauv_nombre_liv.sav";
	begin
		open(F, in_file, nom_fich);
		read (f, t);
		close (f);
		return t;
	end charge_tab_nombre_liv;
	
	
	
	procedure aff_tab_list_liv (tab : in T_tab_list_liv) is
		
	begin
		for i in tab'range loop
			affiche_liv (tab(i));
		end loop;
	end aff_tab_list_liv;
	
	procedure cree_tab_liv (tete : T_pteur_liv; t : out T_tab_list_liv) is
		p : T_pteur_liv := tete;
	begin 
		for i in t'range loop
			t(i) := p.all;
			p := p.suiv;
		end loop;
	end cree_tab_liv;
	
	function cree_list_liv (t : T_tab_list_liv) return T_pteur_liv is
		tete : T_pteur_liv := null;
	begin
		for i in t'range loop
			tete := new T_cell_liv'(t(i).nom,t(i).nb_com_an, t(i).mont_com_tot, tete);
		end loop;
		return tete;
	end cree_list_liv;
	
	procedure sauv_tab_liv (t : in T_tab_list_liv; secteur : T_secteur; statut : T_statut_liv) is
		F : Fich_tab_liv.File_type;
		nom_fich : constant string := ("tab_liv" & T_secteur'image (secteur) & T_statut_liv'image (statut) & ".sav");
	begin
		begin
			open(F, out_file, nom_fich);
			Exception
			When others => Create(F, name=>nom_fich);
		end;
	write(F, t);
	close(F);
	end sauv_tab_liv;
	
	procedure charge_tab_liv (t : out T_tab_list_liv; secteur : T_secteur; statut : T_statut_liv) is
		F : Fich_tab_liv.File_type;
		nom_fich : constant string := ("tab_liv" & T_secteur'image (secteur) & T_statut_liv'image (statut) & ".sav");
	Begin
		open(F, in_file, nom_fich);
		read (f, t);
		close (f);
	end charge_tab_liv;
	
	
	procedure sauv_tous_liv (t : in T_tab_liv) is
		nb_liv : T_tab_nombre_liv;
	begin
		nb_liv := init_tab_nombre_liv (t);
		sauv_tab_nombre_liv (nb_liv);
		for sec in T_secteur'range loop
			for stat in T_statut_liv'range loop
				declare
					list : T_tab_list_liv (1..nb_liv(sec, stat));
				begin
					cree_tab_liv (t(sec, stat), list);
					sauv_tab_liv (list, sec, stat);
				end;
			end loop;
		end loop;
					
	end sauv_tous_liv;
	
	
	function charge_tous_liv return T_tab_liv is 
		t : T_tab_liv;
		nb_liv : T_tab_nombre_liv;
	begin
		begin
		nb_liv := charge_tab_nombre_liv;
		for sec in T_secteur'range loop
			for stat in T_statut_liv'range loop
				declare
					list : T_tab_list_liv (1..nb_liv(sec, stat));
				begin
					charge_tab_liv (list, sec, stat);
					t(sec, stat) := cree_list_liv(list);
				end;
			end loop;
		end loop;
		exception
		When others => Put_line ("Probleme de chargement, livreurs non charges, peut être n'y a t'il pas de sauvegarde ...");
		end;
		return t;
	end charge_tous_liv;
	
end sauvegarde;
