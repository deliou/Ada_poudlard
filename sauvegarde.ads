with gestion_livreur, ada.sequential_io;
use gestion_livreur;

package sauvegarde is

	Type T_tab_list_liv is array (integer range <>) of T_cell_liv;
	package fich_tab_liv is new ada.sequential_io(T_tab_list_liv);
	use fich_tab_liv;
	
	Type T_tab_nombre_liv is array (T_secteur, T_statut_liv) of integer; -- Tableau qui contient le nombre de livreur dans chaque liste.
	package fich_tab_nombre_liv is new ada.sequential_io(T_tab_nombre_liv);
	use fich_tab_nombre_liv;
	
	function init_tab_nombre_liv (tab : in T_tab_liv) return T_tab_nombre_liv;
	procedure aff_tab_nombre_liv (t : T_tab_nombre_liv);
	procedure sauv_tab_nombre_liv (t : in T_tab_nombre_liv);
	function charge_tab_nombre_liv return T_tab_nombre_liv;
	
	procedure aff_tab_list_liv (tab : in T_tab_list_liv); --affiche le tableau de livreur.
	procedure cree_tab_liv (tete : T_pteur_liv; t : out T_tab_list_liv); -- retourne un tableau de livreur à partir d'une liste de livreur.
	function cree_list_liv (t : T_tab_list_liv) return T_pteur_liv; --retourne list à partir du tableau.
	procedure sauv_tab_liv (t : in T_tab_list_liv; secteur : T_secteur; statut : T_statut_liv);
	procedure charge_tab_liv (t : out T_tab_list_liv; secteur : T_secteur; statut : T_statut_liv);
	
	procedure sauv_tous_liv (t : in T_tab_liv); --sauvegarde tous les livreurs d'un coup, dans plusieurs fichiers.
	function charge_tous_liv return T_tab_liv;
	
end sauvegarde;
